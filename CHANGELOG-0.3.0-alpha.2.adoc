= Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

== [0.2.0] - 2024-07-08

=== Added

- Add Ubuntu Noble jobs for Rust

[0.2.0]: https://gitlab.com/ds_2/ci-templates/compare?from=v0.1.20-alpha.4&to=v0.2.0
