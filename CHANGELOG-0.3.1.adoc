= Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

== [0.3.1] - 2024-09-26

=== Added

- Add new stage doc

=== Changed

- Set GOOS and GOARCH globally

[0.3.1]: https://gitlab.com/ds_2/ci-templates/compare?from=v0.3.0-alpha.4&to=0.3.1
