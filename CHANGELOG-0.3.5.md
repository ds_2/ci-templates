# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [0.3.5] - 2025-02-17

### Changed

- Use fixed version for rust DInD image
- Update changelog

[0.3.5]: https://github.com/ds_2/ci-templates/compare/v0.3.4..0.3.5

<!-- generated by git-cliff -->
