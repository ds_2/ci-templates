# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [0.3.6] - 2025-03-04

### Added

- Add expiry for quay.io builds

### Changed

- Update rust jobs to Rust v1.85.0
- Revert "build(kaniko): update pattern for docker config json"
- Update pattern for docker config json
- Set golang to v1.24.0
- Also set fixed version for default rust image
- Idea to use fixed tags on jobs
- Update changelog

[0.3.6]: https://github.com/ds_2/ci-templates/compare/v0.3.5..0.3.6

<!-- generated by git-cliff -->
