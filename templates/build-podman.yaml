.build-with-podman:
  stage: build
  image: quay.io/podman/stable:latest
  variables:
    CTX_DIR: "."
    DKR_FILE: "Containerfile"
    VERSIONLABEL: $CI_COMMIT_TAG
    CI_APPLICATION_REPOSITORY: $CI_REGISTRY_IMAGE/$CI_COMMIT_REF_SLUG
    CI_APPLICATION_TAG: $CI_COMMIT_SHA
    CONTAINER_IMAGE_NAME: $CI_PROJECT_PATH
    CONTAINER_REGISTRY_IMAGE: $CI_REGISTRY/$CONTAINER_IMAGE_NAME
    BUILD_ARGS: ""
    JUST_BUILD: 1
    QUAY_IO_EXPIRY: "never"
    IMAGE_LABELS: >
      --label org.opencontainers.image.vendor=$CI_SERVER_URL/$GITLAB_USER_LOGIN
      --label org.opencontainers.image.authors=$CI_SERVER_URL/$GITLAB_USER_LOGIN
      --label org.opencontainers.image.revision=$CI_COMMIT_SHA
      --label org.opencontainers.image.source=$CI_PROJECT_URL
      --label org.opencontainers.image.documentation=$CI_PROJECT_URL
      --label org.opencontainers.image.licenses=$CI_PROJECT_URL
      --label org.opencontainers.image.url=$CI_PROJECT_URL
      --label vcs-url=$CI_PROJECT_URL
      --label com.gitlab.ci.user=$CI_SERVER_URL/$GITLAB_USER_LOGIN
      --label com.gitlab.ci.email=$GITLAB_USER_EMAIL
      --label com.gitlab.ci.tagorbranch=$CI_COMMIT_REF_SLUG
      --label com.gitlab.ci.pipelineurl=$CI_PIPELINE_URL
      --label com.gitlab.ci.commiturl=$CI_PROJECT_URL/commit/$CI_COMMIT_SHA
      --label com.gitlab.ci.cijoburl=$CI_JOB_URL
      --label com.gitlab.ci.mrurl=$CI_PROJECT_URL/-/merge_requests/$CI_MERGE_REQUEST_ID
  script:
    - if [ ! -f "${CTX_DIR}/$DKR_FILE" ]; then echo "No Dockerfile $DKR_FILE found!"; exit 1; fi
    - echo "Will create image $CONTAINER_REGISTRY_IMAGE with file $DKR_FILE in directory $CTX_DIR"
    - find "$CTX_DIR" -maxdepth 2
    - BUILDDATE="'$(date '+%FT%T%z' | sed -E -n 's/(\+[0-9]{2})([0-9]{2})$/\1:\2/p')'" #rfc 3339 date
    - BUILDTITLE=$(echo $CI_PROJECT_TITLE | tr " " "_")
    - IMAGE_LABELS=${IMAGE_LABELS//[$'\n']/}
    - IMAGE_LABELS="$IMAGE_LABELS --label org.opencontainers.image.created=$BUILDDATE --label build-date=$BUILDDATE"
    - IMAGE_LABELS="$IMAGE_LABELS --label quay.expires-after=${QUAY_IO_EXPIRY}"
    - IMAGE_LABELS="$IMAGE_LABELS --label org.opencontainers.image.title=$BUILDTITLE --label org.opencontainers.image.description=$BUILDTITLE"
    - IMAGE_LABELS="$IMAGE_LABELS --label org.opencontainers.image.ref.name=$CONTAINER_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG"
    - if [ -z "$VERSIONLABEL" ]; then VERSIONLABEL=$CI_COMMIT_REF_SLUG; fi
    - if [[ ! -z "$VERSIONLABEL" ]]; then IMAGE_LABELS="$IMAGE_LABELS --label org.opencontainers.image.version=$VERSIONLABEL"; ADDITIONALTAGLIST="$ADDITIONALTAGLIST $VERSIONLABEL"; fi
    - ADDITIONALTAGLIST="$ADDITIONALTAGLIST $CI_COMMIT_REF_SLUG $CI_COMMIT_SHORT_SHA"
    - |
      if [[ "$CI_COMMIT_BRANCH" == "$CI_DEFAULT_BRANCH" ]]; then
        ADDITIONALTAGLIST="$ADDITIONALTAGLIST latest"
      fi
    - |
      if [[ ! -z "$VERSIONLABEL" ]]; then
        IMAGE_LABELS="$IMAGE_LABELS --label org.opencontainers.image.version=$VERSIONLABEL"
      fi
    - CMD="podman build"
    - CMD="$CMD --file $CTX_DIR/${DKR_FILE}"
    - |
      if [[ -n "$ADDITIONALTAGLIST" ]]; then
        for TAG in $ADDITIONALTAGLIST; do
          FORMATTEDTAGLIST="${FORMATTEDTAGLIST} --tag $CONTAINER_REGISTRY_IMAGE:$TAG ";
        done
      fi
    - |
      ## set build args!!
      if [ ! -z "$BUILD_ARGS" ]; then
        for arg in $BUILD_ARGS; do
          echo "- adding build arg: $arg"
          CMD="$CMD --build-arg $arg"
        done
      fi
    - |
      if [[ $JUST_BUILD -eq 1 ]]; then
        echo "will perform build"
      else
        echo "will perform push after build"
        podman login "$CI_REGISTRY" --username "$CI_REGISTRY_USER" --password "$CI_REGISTRY_PASSWORD"
      fi
    - CMD="$CMD $IMAGE_LABELS"
    - CMD="$CMD $KANIKO_CACHE_ARGS"
    - CMD="$CMD $FORMATTEDTAGLIST"
    - CMD+=" --squash-all"
    - CMD+=" $CTX_DIR"
    - |
      echo "Cmd will be: $CMD"
    - eval "$CMD"
    - |
      if [[ ! $JUST_BUILD -eq 1 ]]; then
        echo "Pushing to targets"
        if [[ -n "$ADDITIONALTAGLIST" ]]; then
          for TAG in $ADDITIONALTAGLIST; do
            podman push $CONTAINER_REGISTRY_IMAGE:$TAG
          done
        fi
      fi

# ref: https://gitlab.com/guided-explorations/containers/kaniko-docker-build/-/blob/master/.gitlab-ci.yml
.deploy-with-podman:
  extends: .build-with-podman
  stage: deploy
  variables:
    JUST_BUILD: 0
